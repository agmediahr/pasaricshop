<?php


namespace Agmedia\Luceed\Connection;


use Agmedia\Helpers\Log;
use GuzzleHttp\Exception\RequestException;

/**
 * Class LuceedService
 * @package Agmedia\Luceed\Connection
 */
class LuceedService
{

    /**
     * @var mixed|string
     */
    private $base_url;

    /**
     * @var mixed|string
     */
    private $username;

    /**
     * @var mixed|string
     */
    private $password;

    /**
     * @var string
     */
    public $env = 'production';


    /**
     * LuceedService constructor.
     */
    public function __construct()
    {
        $this->base_url = agconf('service.base_url');
        $this->username = agconf('service.username');
        $this->password = agconf('service.password');
    }


    /**
     * @param string $url
     * @param string $option
     *
     * @return mixed
     */
    public function get(string $url, string $option = '')
    {
        // Local or testing enviroment.
        if ($this->env == 'local') {
            return file_get_contents(DIR_UPLOAD . 'luceed_json/' . $url);
        }

        // If it's not local,
        // It's production or live.
        try {
            $ch = curl_init($this->base_url . $url . $option);
            curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . agconf('service.password'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            return curl_exec($ch);
        } catch (\Exception $exception) {
            Log::error($exception);
        }
    }


    /**
     * @param string $url
     * @param array  $body
     *
     * @return mixed
     */
    public function post(string $url, array $body)
    {

    }


    /**
     * @param string $type
     * @param        $exception
     */
    private function log(string $type, $exception)
    {
        Log::test($type, 'luceed_service_error');
        Log::test($exception->getMessage(), 'luceed_service_error');
    }
}