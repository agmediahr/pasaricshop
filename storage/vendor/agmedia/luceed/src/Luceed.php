<?php


namespace Agmedia\Luceed;


use Agmedia\Luceed\Connection\LuceedService;

/**
 * Class Luceed
 * @package Agmedia\Luceed
 */
class Luceed
{
    
    /**
     * @var LuceedService
     */
    private $service;

    /**
     * @var array
     */
    private $end_points = [
        // Test urls / files.
        /*'group_list' => 'grupeartikala_lista.json',
        'product_list' => 'artikli_atribut_uid.json',
        'product_image' => 'product_image.json',
        'manufacturer_uid' => 'partner_single.json',*/
        // Live urls.
        'group_list' => 'grupeartikala/lista',
        'product_list' => 'artikli/atribut/atribut_uid/59-2987',
        'product_image' => 'artikli/dokumenti/',
        'manufacturer_uid' => 'partneri/uid/',
    ];
    
    
    /**
     * Luceed constructor.
     */
    public function __construct()
    {
        $this->service = new LuceedService();
    }
    
    
    /*******************************************************************************
     *                                Copyright : AGmedia                           *
     *                              email: filip@agmedia.hr                         *
     *******************************************************************************/
    // GROUPS / CATEGORIES
    
    /**
     * @return mixed
     */
    public function getGroupList()
    {
        return $this->service->get($this->end_points['group_list']);
    }
    
    
    /**
     * @return mixed
     */
    public function getGroupAdditionsList()
    {
        return $this->service->get('grupepartikala/dodaci/lista/');
    }
    
    
    /**
     * @param string $group_uid
     *
     * @return mixed
     */
    public function getGroup(string $group_uid)
    {
        return $this->service->get('grupeartikala/sifra/', $group_uid);
    }
    
    
    /*******************************************************************************
     *                                Copyright : AGmedia                           *
     *                              email: filip@agmedia.hr                         *
     *******************************************************************************/
    // MANUFACTURERS
    
    /**
     * @return mixed
     */
    public function getManufacturerList()
    {
        return $this->service->get('partneri/lista/');
    }
    
    
    /**
     * @return mixed
     */
    public function getManufacturer(string $manufacturer_uid)
    {
        return $this->service->get($this->end_points['manufacturer_uid']/* . $manufacturer_uid*/);
    }
    
    
    /*******************************************************************************
     *                                Copyright : AGmedia                           *
     *                              email: filip@agmedia.hr                         *
     *******************************************************************************/
    // PRODUCTS
    
    /**
     * @param array $query
     *
     * @return mixed
     */
    public function getProductsList(array $query = [])
    {
        return empty($query) ? $this->service->get($this->end_points['product_list']) : $this->service->get($this->end_points['product_list'] . $query);
    }
    
    
    /**
     * @param string|array $group_id
     *
     * @return mixed
     */
    public function getGroupProducts($group_id)
    {
        return $this->service->get('artikli/GrupaArtikala/', $group_id);
    }
    
    
    /**
     * @param string|array $manufacturer_id
     *
     * @return mixed
     */
    public function getManufacturerProducts($manufacturer_id)
    {
        return $this->service->get('artikli/RobnaMarka/', $manufacturer_id);
    }
    
    
    /**
     * @param string $value
     * @param string $type
     *
     * @return mixed
     */
    public function getProduct(string $value, string $type = 'sifra')
    {
        return $this->service->get('artikli/' . $type . '/', $value);
    }
    
    
    /**
     * @return mixed
     */
    public function getProductsActions()
    {
        return $this->service->get('akcije/lista/');
    }
    
    
    /**
     * @param string $uid
     *
     * @return mixed
     */
    public function getProductImage($uid)
    {
        return $this->service->get($this->end_points['product_image'], $uid);
    }
    
    
    /**
     * @param null|string|array $units
     *
     * @return mixed
     */
    public function getStock($units = null)
    {
        return $units ? $this->service->get('StanjeZalihe/Skladiste/' . $units) : $this->service->get('StanjeZalihe/Skladiste/');
    }
    
    
    /**
     * @param                   $product_id
     * @param null|string|array $units
     *
     * @return mixed
     */
    public function getProductStock($product_id, $units = null)
    {
        return $units ? $this->service->get('StanjeZalihe/Artikl/' . $product_id . '/', $units ) : $this->service->get('StanjeZalihe/Artikl/', $product_id);
    }
    
    
    /*******************************************************************************
     *                                Copyright : AGmedia                           *
     *                              email: filip@agmedia.hr                         *
     *******************************************************************************/
    // CUSTOMERS
    
    /**
     * @return mixed
     */
    public function getCustomers()
    {
        return $this->service->get('partneri');
    }
    
    
    /**
     * @param string $value
     * @param string $type
     *
     * @return mixed
     */
    public function getCustomer(string $value, string $type = 'sifra')
    {
        return $this->service->get('partneri/' . $type . '/', $value);
    }
    
    
    /**
     * @param array $customer
     *
     * @return mixed
     */
    public function createCustomer(array $customer)
    {
        return $this->service->post('partneri/snimi/', $customer);
    }
    
    
    /**
     * @param string $partner_uid
     * @param array  $body
     *
     * @return mixed
     */
    public function updateCustomer(string $partner_uid, array $body)
    {
        $body['partner_uid'] = $partner_uid;
        
        return $this->service->post('partneri/snimi/', $partner_uid);
    }
    
    
    /*******************************************************************************
     *                                Copyright : AGmedia                           *
     *                              email: filip@agmedia.hr                         *
     *******************************************************************************/
    // ORDERS
    
    /**
     * @param array $order
     *
     * @return mixed
     */
    public function createOrder(array $order)
    {
        return $this->service->post('NaloziProdaje/snimi/', $order);
    }
    
    
    /**
     * @param string $order_uid
     *
     * @return mixed
     */
    public function getOrder(string $order_uid)
    {
        return $this->service->get('NaloziProdaje/uid/', $order_uid);
    }
    
    
    /**
     * @param array $status
     *
     * @return mixed
     */
    public function getOrdersByStatus(array $status)
    {
        return $this->service->get('NaloziProdaje/statusi/', $status);
    }
    
}