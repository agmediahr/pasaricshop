<?php
// Heading
$_['title']         = '<span><i class="fa fa-cogs" style="margin-right: 12px;"></i>Luceed Sync. Manager</span>';
$_['heading_title'] = 'Luceed Sync. Manager';

// Text
$_['text_extension']          = 'Modules';
$_['text_success_categories'] = 'Success: %s new categories added!';
$_['text_warning_categories'] = 'Warning: No new categories to add!';
$_['text_success_products'] = 'Success: %s new products added!';
$_['text_warning_products'] = 'Warning: No new products to add!';
$_['text_success_manufacturers'] = 'Success: %s new manufacturer added!';
$_['text_warning_manufacturers'] = 'Warning: No new manufacturer to add!';

// Btn
$_['btn_products_import']     = 'Products';
$_['btn_categories_import']   = 'Categories';
$_['btn_manufacturer_import'] = 'Manufacturers';

// Helper
$_['help_products_import']   = 'Import only the new products. Old ones will not be changed!';
$_['help_categories_import'] = 'Import only the new categories.<br>Old ones will not be afected!';
$_['help_manufacturer_import'] = 'Import only the new manufacturers.<br>Old ones will not be afected!';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Luceed Sync. Manager module!';

