<?php
// AGmedia Custom
define('OC_ENV', [
    'shipping_collector_defaults' => [
        0 => [
            'time' => '11-16h',
            'max'  => '35'
        ],
        1 => [
            'time' => '18-21h',
            'max'  => '40'
        ]
    ],
    'service'                     => [
        // test_url http://luceedapi-test.tomsoft.hr:3676/datasnap/rest/
        // live_url http://luceedapi.tomsoft.hr:3675/datasnap/rest/
        'base_url' => 'http://luceedapi-test.tomsoft.hr:3676/datasnap/rest/',
        'username' => 'webshop_prehrana',
        'password' => 'i8Qhb152',
    ],
    'import'                      => [
        'default_category'    => 0,
        'default_language'    => 2, // HR
        'default_tax_class'   => 11, // PDV
        'default_stock_empty' => 5,
        'default_stock_full'  => 7,
        'image_path'          => 'catalog/products/'
    ]
]);